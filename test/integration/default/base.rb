describe user('postgres') do
  it { should exist }
end

describe group('postgres') do
  it { should exist }
end

describe file('/usr/bin/postgres_exporter') do
  its('type') { should cmp 'file' }
  it { should be_file }
  it { should_not be_directory }
end

describe file('/opt/postgres_exporter/') do
 its('type') { should eq :directory }
 it { should be_directory }
end

describe file('/etc/default/postgres_exporter') do
  its('type') { should cmp 'file' }
  it { should be_file }
  it { should be_owned_by 'root' }
  it { should_not be_directory }
end

describe file('/etc/postgres_exporter') do
 it { should exist }
 it { should be_owned_by 'postgres' }
end

describe file('/etc/postgres_exporter/queries.yml') do
  its('type') { should cmp 'file' }
  it { should be_file }
  it { should_not be_directory }
  it { should be_owned_by 'postgres' }
end

describe service('postgres_exporter') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end
