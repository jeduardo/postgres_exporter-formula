{% from slspath + '/map.jinja' import postgres_exporter with context %}

postgres_exporter-create-user:
  user.present:
    - name: {{ postgres_exporter.service.user }}
    - shell: /bin/false
    - system: True
    - home: /dev/null
    - createhome: False
    - unless: getent passwd | grep {{ postgres_exporter.service.user }}

postgres_exporter-create-group:
  group.present:
    - name: {{ postgres_exporter.service.group }}
    - system: True
    - members:
      - {{ postgres_exporter.service.user }}
    - require:
      - user: {{ postgres_exporter.service.user }}
    - unless: getent group | grep {{ postgres_exporter.service.group }}

postgres_exporter-bin-dir:
  file.directory:
   - name: {{ postgres_exporter.service.bin_dir }}
   - makedirs: True

postgres_exporter-dist-dir:
 file.directory:
   - name: {{ postgres_exporter.service.dist_dir }}
   - makedirs: True

postgres_exporter-install-binary:
 archive.extracted:
   - name: {{ postgres_exporter.service.dist_dir }}/postgres_exporter-{{ postgres_exporter.service.version }}
   - source: https://github.com/wrouesnel/postgres_exporter/releases/download/v{{ postgres_exporter.service.version }}/postgres_exporter_v{{ postgres_exporter.service.version }}_{{ grains['kernel'] | lower }}-{{ postgres_exporter.service.arch }}.tar.gz
   - skip_verify: True
   - options: --strip-components=1
   - user: {{ postgres_exporter.service.user }}
   - group: {{ postgres_exporter.service.group }}
   - enforce_toplevel: False
   - unless:
     - '[[ -f {{ postgres_exporter.service.dist_dir }}/postgres_exporter-{{ postgres_exporter.service.version }}/postgres_exporter ]]'
 file.symlink:
   - name: {{ postgres_exporter.service.bin_dir }}/postgres_exporter
   - target: {{ postgres_exporter.service.dist_dir }}/postgres_exporter-{{ postgres_exporter.service.version }}/postgres_exporter
   - mode: 0755
   - unless:
     - '[[ -f {{ postgres_exporter.service.bin_dir }}/postgres_exporter ]] && {{ postgres_exporter.service.bin_dir }}/postgres_exporter -v | grep {{ postgres_exporter.service.version }}'