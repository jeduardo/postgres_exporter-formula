{% from slspath + '/map.jinja' import postgres_exporter with context %}

postgres_exporter-create-env-file:
  file.managed:
    - name: {{ postgres_exporter.service.env_file }}
    - makedirs: True
    - user: root
    - group: root
    - mode: 400
    - contents: |
      {%- for key, value in postgres_exporter.config.items() %}
        {{ key }}="{{ value }}"
      {%- endfor %}

postgres_exporter-create-queries-file:
  file.serialize:
    - name: {{ postgres_exporter.config.PG_EXPORTER_EXTEND_QUERY_PATH }}
    - formatter: yaml
    - makedirs: True
    - user: {{ postgres_exporter.service.user }}
    - group: {{ postgres_exporter.service.group }}
    - mode: 400
    - dataset: {{ postgres_exporter.queries }}
    - serializer_opts:
      - width: 400

postgres_exporter-install-service:
  file.managed:
    - name: /etc/systemd/system/postgres_exporter.service
    - source:  salt://{{ slspath }}/files/exporter.service.j2
    - template: jinja
    - context:
        postgres_exporter: {{ postgres_exporter }}
    - require: 
      - file: postgres_exporter-create-env-file
      - file: postgres_exporter-create-queries-file

postgres_exporter-service:
  service.running:
    - name: postgres_exporter
    - enable: True
    - start: True
    - watch:
      - file: postgres_exporter-install-service
      - file: postgres_exporter-create-env-file
      - file: postgres_exporter-create-queries-file

